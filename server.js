const express = require('express');
const app = express();
const fs = require('fs');

const cors = require('cors'); // Importa cors
const jwt = require('jsonwebtoken');
const port = 8080;
const SECRET_KEY = 'hide-key';
const databaseFile = './database.sqlite';
const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database('./database.sqlite');
const Sentry = require("@sentry/node");

Sentry.init({
  dsn: "https://ce44baa7bed848fd8acdf2635b6e79e8@o305110.ingest.sentry.io/4505135868411904",

  // We recommend adjusting this value in production, or using tracesSampler
  // for finer control
  tracesSampleRate: 1.0,
});

const transaction = Sentry.startTransaction({
  op: "test",
  name: "My First Test Transaction",
});

setTimeout(() => {
  try {
    foo();
  } catch (e) {
    Sentry.captureException(e);
  } finally {
    transaction.finish();
  }
}, 99);

function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];
  
    if (!token) {
      return res.status(401).json({ message: 'No token provided' });
    }
  
    jwt.verify(token, SECRET_KEY, (err, user) => {
      if (err) {
        return res.status(403).json({ message: 'Invalid token' });
      }
  
      req.user = user;
      next();
    });
}

db.run(
    `CREATE TABLE IF NOT EXISTS tasks (
      id INTEGER PRIMARY KEY,
      user_id INTEGER,
      name TEXT NOT NULL,
      description TEXT,
      FOREIGN KEY (user_id) REFERENCES users (id)
    )`,
    (err) => {
      if (err) {
        console.error(err);
      } else {
        console.log('Tasks table created successfully');
        db.get('SELECT * FROM tasks WHERE user_id = ?', ['1'], (err, row) => {
            if (err) {
                console.error(err);
                return;
            }
        
            if (!row) {
                const userId = 1;
        
                const tasks = [
                  { name: 'Task 1', description: 'Description for Task 1' },
                  { name: 'Task 2', description: 'Description for Task 2' },
                  { name: 'Task 3', description: 'Description for Task 3' },
                ];
            
                tasks.forEach(({ name, description }) => {
                  db.run(
                    'INSERT OR IGNORE INTO tasks (user_id, name, description) VALUES (?, ?, ?)',
                    [userId, name, description],
                    (err) => {
                      if (err) {
                        console.error(err);
                      }
                    }
                  );
                });
        
                console.log('Sample tasks added for user "1"');
            } else {
                console.log('User "test" already exists.');
            }
        });        
      }
    }
);

app.use(cors()); // Habilita CORS para todas las rutas
app.use(express.json());

app.get('/', (req, res) => {
    res.send('Hello, World svc-tasks DEMO2!');
});
  
app.get('/tasks', authenticateToken, (req, res) => {
    const userId = req.user.id;

    db.all('SELECT * FROM tasks WHERE user_id = ?', [userId], (err, rows) => {
        if (err) {
        console.error(err);
        return res.status(500).json({ message: 'Internal server error' });
        }

        res.status(200).json(rows);
    });
});

fs.access(databaseFile, fs.constants.F_OK, (err) => {
    if (err) {
        console.log('Database file not found. Creating a new one...');
        fs.writeFileSync(databaseFile, '');
        console.log('Database file created.');
    } else {
        console.log('Database file found.');
    }

    app.listen(port, () => {
        console.log(`Server is listening at http://localhost:${port}`);
    });
});